// use std::path::PathBuf;
use clap::{Arg, Command};  // create and parse CLI arguments
// use argfile;              // for loading additional arguments from a file
use std::io::{self, Write};
use std::io::prelude::*;
use atty;                  // to make sure we are not in tty
use regex::Regex;   // https://docs.rs/regex/latest/regex/index.html

struct ANSI_spec {
    protect_color : bool,
    regex         : Regex
}

// TODO: create a build file to do the following
//      - TODO: shell completion. Example: https://github.com/BurntSushi/ripgrep/blob/0b36942f680bfa9ae88a564f2636aa8286470073/build.rs#L39
// TODO: detect the previous color tag
//      - TODO: if the color tag was immediately before the text, modify it and replace it
//      - TODO: if the color tag was not immediately before the target text, mirror it after instead of re-setting the color
// TODO: make the non-UTF-8 text also work. The current limitation most probably comes form String ensuring its data will always be valid UTF-8


// a function to add the color coding based on regex
fn func_colorise(ANSI_settings: &ANSI_spec, e: &String, col: &str, buf: &String) -> String {
    // println!("{:?} -- {:?} -- {:?}", e, col, buf);
    
    let re = Regex::new(&e).unwrap();
    let lead: &str = "\x1b[0;";
    
    // let position = re.find(&buf).unwrap();
    // println!("{:?}", position.start());
    
    let mut result: String;
    
    if re.is_match(&buf) {

        // Locate color sequences if protect_color was set to true
        let mut tmp_cc_pos_start = Vec::new();
        let mut tmp_cc_pos_end = Vec::new();
        if (ANSI_settings.protect_color) {
            if ANSI_settings.regex.is_match(&buf) {
                for pos in ANSI_settings.regex.find_iter(&buf) {
                    let mut tmp_start = vec![pos.start()];
                    let mut tmp_end = vec![pos.end()];
                    tmp_cc_pos_start.append(&mut tmp_start);
                    tmp_cc_pos_end.append(&mut tmp_end);
                }
            }
        }
        result = buf.to_string();
        
        let mut tmp_pos_start = Vec::new();
        let mut tmp_pos_end = Vec::new();
        
        for pos in re.find_iter(&buf) {
            let mut tmp_start = vec![pos.start()];
            let mut tmp_end = vec![pos.end()];
            
            tmp_pos_start.append(&mut tmp_start);
            tmp_pos_end.append(&mut tmp_end);
        }
        tmp_pos_start.reverse();
        tmp_pos_end.reverse();
        
        
        for indx in 0..tmp_pos_start.len() {
            // result.insert_str(&start_pos, &col);
            //println!("{}{}", &lead, &col);
            
            let target = &result.get(tmp_pos_start[indx]..tmp_pos_end[indx]);
            let colored_target = lead.to_string() + &col.to_string() + target.as_deref().unwrap() + "\x1b[0m";

            // protect color sequences if protect_color was set to true
            let mut acceptable = true;
            let mut ctrlcode = 0;
            if (ANSI_settings.protect_color) {
            while (ctrlcode < tmp_cc_pos_start.len() && acceptable) {
                if (
                    (tmp_pos_start[indx] <= (tmp_cc_pos_end[ctrlcode]-1) && tmp_pos_start[indx] >= tmp_cc_pos_start[ctrlcode]) ||
                    ((tmp_pos_end[indx]-1) <= (tmp_cc_pos_end[ctrlcode]-1) && (tmp_pos_end[indx]-1) >= tmp_cc_pos_start[ctrlcode])
                   ) {
                    acceptable = false;
                }
                ctrlcode = ctrlcode + 1;
            }
            }
            if (acceptable) {
                result.replace_range(tmp_pos_start[indx]..tmp_pos_end[indx], &colored_target);
            }
        }
        
        
        // result = re.replace_all(&buf, &col).to_string();
    } else {
        result = buf.to_string();
    }
    
    return result.to_string();
}


fn func_error(msg: &[u8]) {
    let stderr = io::stderr();
    let mut handle = stderr.lock();
    
    handle.write_all(msg);
    
    std::process::exit(1);
}


// fn main() -> io::Result<()> {
fn main() {
    
    //-------[ create arguments ]-------//
    let matches = Command::new("highlighter")
        .author("Mehrad Mahmoudian, mehrad@mahmoudian.me")
        .version("0.0.1")
        .about("Highlight piped text with different colors based on patterns")
        .long_about(
            "This program reads input from standard input (stdin), applies the highlighting based on the provided regular expressions and defined colors and returns the output to standard output (stdout).")
        // TODO: write the mode part (read the regexes from a file)
        //.arg(
        //    Arg::new("mode")
        //        .long("mode")
        //        // .conflicts_with("search")
        //        .multiple_values(false)
        //        .help("A pre-defined highlighting mode")
        //        .takes_value(true)
        //)
        .arg(Arg::new("white").long("white").short('w').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("whitebg").long("whitebg").short('W').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("black").long("black").short('k').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("blackbg").long("blackbg").short('K').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("red").long("red").short('r').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("redbg").long("redbg").short('R').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("green").long("green").short('g').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("greenbg").long("greenbg").short('G').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("yellow").long("yellow").short('y').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("yellowbg").long("yellowbg").short('Y').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("blue").long("blue").short('b').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("bluebg").long("bluebg").short('B').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("magenta").long("magenta").short('m').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("magentabg").long("magentabg").short('M').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .arg(Arg::new("cyan").long("cyan").short('c').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        .arg(Arg::new("cyanbg").long("cyanbg").short('C').required(false).multiple_occurrences(true).takes_value(true).hide(true))
        
        .after_help("This program is licensed under GPL 3.0 or later. \
                     please report bugs or feature requests to : \
                     https://github.com/mmahmoudian/highlighter ")
        
        // .override_help("myapp v1.0\n\
        //    Does awesome things\n\
        //    (C) me@mail.com\n\n\
        //    
        //    USAGE: myapp <opts> <command>\n\n\
        //    
        //    Options:\n\
        //    -h, --help       Display this message\n\
        //    -V, --version    Display version info\n\
        //    -s <stuff>       Do something with stuff\n\
        //    -v               Be verbose\n\n\
        //    
        //    Commands:\n\
        //    help             Print this message\n\
        //    work             Do some work")
        .get_matches();
    
    
    //-------[ some internal initial variables ]-------//
    // https://bixense.com/clicolors/
    let color_names = vec!["white", "black", "red", "green", "yellow", "blue", "magenta", "cyan"];
    // TODO: replace it with something like `paste0(30:37, "m")`
    let fg_colors = vec!["30m", "31m", "32m", "33m", "34m", "35m", "36m", "37m"];
    let bg_colors = vec!["40m", "41m", "42m", "43m", "44m", "45m", "46m", "47m"];
    // specify how we recognize ANSI sequences. Why is this defined here? Because the lifetime
    // of this regex is multiple calls to func_colorise.
    let ANSI_settings = ANSI_spec {
        protect_color: true,
        regex: Regex::new("\x1B.[^m]*m").unwrap()
    };
    
    // make sure something is piped to this software
    if atty::is(atty::Stream::Stdin) {
        // println!("Nothing is piped to rhl. Please read --help");
        // std::io::Error::new(std::io::ErrorKind::Other, "Nothing is piped to rhl. Please read --help");
        // return;
        
        
        // let stderr = io::stderr();
        // let mut handle = stderr.lock();
        // handle.write_all(b"Nothing is piped to rhl. Please read --help").unwrap();
        // Ok(String::new());
        
        // Err(String::from("Nothing is piped to rhl. Please read --help"));
        
        // panic!("Nothing is piped to rhl. Please read --help");
        
        
        func_error(b"Nothing is piped to highlighter. Please read --help\n");
    }
    
    
    //-------[ read the standard input ]-------//
    //let mut buffer = String::new();
    
    // We get `Stdin` here
    let stdin = io::stdin(); 
    
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    
    for line in stdin.lock().lines() {
        //println!("{}", line.unwrap());
        let mut buffer = line.unwrap();
        
        // if length of line was zero, skip
        if buffer.len() == 0 {
            handle.write_all(b"\n");
            continue;
        }
        
        // iterate through colors
        for col in color_names.iter(){
            // if this color is defined by user
            if matches.is_present(col) {
                // get the regex user provided for this color
                let tmp_regex: &str = matches.value_of(col).expect("required");
                
                // find the index of this color
                let col_index = color_names
                                        .iter()
                                        .position(|&x| x == col.to_string())
                                        .unwrap();
                
                // get the code for this color
                let tmp_color: &str = fg_colors[col_index-1];
                
                buffer = func_colorise(&ANSI_settings, &tmp_regex.to_string(), &tmp_color, &buffer.to_string());
            }
        }
        
        
        // TODO: repeat the same for loop above for background colors
        
        
        // output the final modified buffer
        //println!("{}", buffer);
        
        
        handle.write_all(&buffer.to_string().as_bytes());
        handle.write_all(b"\n");
    }
    
    
    
    
    std::process::exit(0);
    
    
    // Ok(&buffer);
}


